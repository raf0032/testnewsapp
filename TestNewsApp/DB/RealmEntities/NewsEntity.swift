//
//  NewsEntity.swift
//  TestNewsApp
//
//  Created by Rafael Nalbandyan on 2/27/21.
//

import Foundation
import RealmSwift

final class NewsEntity: Object {
    @objc dynamic var _id: ObjectId = ObjectId.generate()
    @objc dynamic var category = ""
    @objc dynamic var title = ""
    @objc dynamic var body = ""
    @objc dynamic var shareUrl = ""
    @objc dynamic var coverPhotoUrl = ""
    @objc dynamic var date: Date = Date(timeIntervalSince1970: 1)
    let gallery = List<GalleryEntity>()
    @objc dynamic var seen: Bool = false
    
    convenience init(category: String,
                     title: String,
                     body: String,
                     coverPhotoUrl: String,
                     shareUrl: String,
                     date: Date) {
        self.init()
        self.category = category
        self.title = title
        self.body = body
        self.coverPhotoUrl = coverPhotoUrl
        self.shareUrl = shareUrl
        self.date = date
    }
    
    override static func primaryKey() -> String? {
        return "_id"
    }
}
