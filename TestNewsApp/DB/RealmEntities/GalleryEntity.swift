//
//  GalleryEntity.swift
//  TestNewsApp
//
//  Created by Rafael Nalbandyan on 2/27/21.
//

import Foundation
import RealmSwift

final class GalleryEntity: Object {
    @objc dynamic var thumbnailUrl: String = ""
    @objc dynamic var contentUrl: String = ""
    let news = LinkingObjects(fromType: NewsEntity.self, property: "gallery")

    convenience init(thumbnailUrl: String, contentUrl: String) {
        self.init()
        self.thumbnailUrl = thumbnailUrl
        self.contentUrl = contentUrl
    }
}
