//
//  NewsTableViewCell.swift
//  TestNewsApp
//
//  Created by Rafael Nalbandyan on 2/26/21.
//

import UIKit

protocol NewsTableViewCellDelegate: class {
    func updateCell(cell: NewsTableViewCell)
}

final class NewsTableViewCell: UITableViewCell {
    public static let id: String = "NewsTableViewCell"
    
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var newsTitleLabel: UILabel!
    @IBOutlet weak var newsCategoryLabel: UILabel!
    @IBOutlet weak var newsDateLabel: UILabel!
    @IBOutlet weak var seenView: UIView!
    
    weak var delegate: NewsTableViewCellDelegate?
    
    func setUpCell(dataNetwork: DataNews) {
        self.newsTitleLabel.text = dataNetwork.title ?? "-"
        self.newsImageView.image = nil
        self.newsCategoryLabel.text = dataNetwork.category ?? "-"
        DispatchQueue.main.async {
            self.newsImageView.setImage(fromString: dataNetwork.coverPhotoUrl)
        }
        setUpDate(date: dataNetwork.date)
    }
    
    func setUpCell(dataStorage: NewsEntity) {
        self.newsTitleLabel.text = dataStorage.title
        self.newsImageView.image = nil
        self.newsCategoryLabel.text = dataStorage.category
        DispatchQueue.main.async {
            self.newsImageView.setImage(fromString: dataStorage.coverPhotoUrl)
        }
    }
    
    private func setUpDate(date: Int?) {
        guard let data = date else { return }
        let newsDate = Date(timeIntervalSince1970: TimeInterval(data))
        self.newsDateLabel.text = newsDate.toString(dateFormat: "HH:mm E, d MMM y")
    }
}
