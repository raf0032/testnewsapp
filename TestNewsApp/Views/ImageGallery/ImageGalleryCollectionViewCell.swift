//
//  ImageGalleryCollectionViewCell.swift
//  TestNewsApp
//
//  Created by Rafael Nalbandyan on 2/27/21.
//

import UIKit

final class ImageGalleryCollectionViewCell: UICollectionViewCell {
    public static let id = "ImageGalleryCollectionViewCell"
    @IBOutlet weak var newsImageView: UIImageView!
    
    func setUpCell(gallery: DataGallery) {
        self.newsImageView.image = nil
        DispatchQueue.main.async {
            self.newsImageView.setImage(fromString: gallery.thumbnailUrl)
        }
    }
}
