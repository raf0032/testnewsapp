//
//  VideoCollectionViewCell.swift
//  TestNewsApp
//
//  Created by Rafael Nalbandyan on 3/2/21.
//

import UIKit
import AVFoundation
import AVKit

final class VideoCollectionViewCell: UICollectionViewCell {
    static let id: String = "VideoCollectionViewCell"
    @IBOutlet weak var videoView: UIView!
    @IBOutlet var shotImageView: UIImageView!
    
    func setUpCell(video: DataVideo) {
        self.videoView = nil
        guard let videoBackgroundImageUrl = video.thumbnailUrl else { return }
        DispatchQueue.main.async {
            self.shotImageView.setImage(fromString: videoBackgroundImageUrl)
        }
    }

    func playVideo(videoUrl: String, complition: @escaping(_ avController: AVPlayerViewController, _ player: AVPlayer) -> Void) {
        guard let url = URL(string: videoUrl) else { return }
        let player = AVPlayer(url: url)
        let controller = AVPlayerViewController()
        controller.player = player
        complition(controller, player)
    }
}



