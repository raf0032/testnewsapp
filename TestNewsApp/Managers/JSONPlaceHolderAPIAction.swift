//
//  Endpoint.swift
//  TestNewsApp
//
//  Created by Rafael Nalbandyan on 2/27/21.
//

import Foundation
import Alamofire

enum JSONPlaceHolderAPIAction {
    case getNews
}

extension JSONPlaceHolderAPIAction: APIRouter {
    
    var actionParameters: [String : Any] {
        [:]
    }
    
    var encoding: ParameterEncoding {
        return URLEncoding.default
    }
    
    var authHeader: HTTPHeaders? {
        return [:]
    }
    
    var method: HTTPMethod {
        switch self {
        case .getNews:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .getNews:
            return Constants.Rooting.getNews
        }
    }
    
    var baseURL: String {
        return Constants.endPoint
    }
}
