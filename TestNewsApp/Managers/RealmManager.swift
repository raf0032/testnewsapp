//
//  RealmManager.swift
//  TestNewsApp
//
//  Created by Rafael Nalbandyan on 2/27/21.
//

import Foundation
import RealmSwift

final class RealmManager {
    var realm: Realm?
    
    public func getData() -> [NewsEntity] {
        let realm: Realm = try! Realm()
        let data = Array(realm.objects(NewsEntity.self))
        return data
    }
    
    public func saveData(_ news: DataNews, complition: @escaping (NewsEntity) -> Void) {
        let realm: Realm = try! Realm()
        do {
            try realm.write {
                let item = NewsEntity()
                item.body = news.body ?? ""
                item.category = news.category ?? ""
                item.title = news.title ?? ""
                item.shareUrl = news.shareUrl ?? ""
                item.coverPhotoUrl = news.coverPhotoUrl ?? ""
                
                if let gallery = news.gallery {
                    for i in gallery {
                        let dataGallery = GalleryEntity()
                        dataGallery.contentUrl = i.contentUrl ?? ""
                        dataGallery.thumbnailUrl = i.thumbnailUrl ?? ""
                        item.gallery.append(objectsIn: [dataGallery])
                    }
                }
                realm.add(item)
                complition(item)
            }
        } catch {
            print(error)
        }
    }
    
    public func deleteData() {
        let realm: Realm = try! Realm()
        do {
            try realm.write {
                realm.deleteAll()
            }
        } catch {
            print(error)
        }
    }
    
    func updateById(id: ObjectId, fieldName: String, fieldValue: Any ) {
        let realm = try! Realm()
        do {
            let data = try findDataById("_id", value: id)
            try realm.write {
                data.setValue(fieldValue, forKey: fieldName)
            }
        } catch {
            print(error)
        }
    }
    
    private func findDataById(_ field: String, value: ObjectId) throws -> Results<NewsEntity> {
        let realm = try! Realm()

        let predicate = NSPredicate(format: "%K = %@", field, value)
        let result = realm.objects(NewsEntity.self).filter(predicate)
        return result
    }
    
    public func deleteSchemas() {
        let path = Realm.Configuration.defaultConfiguration.fileURL!
        do {
            try FileManager.default.removeItem(at: path)
        } catch {
            print(error)
        }
    }
}

enum RuntimeError: Error {
    case NoRealmSet
}
