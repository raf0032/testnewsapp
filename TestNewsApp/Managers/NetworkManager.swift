//
//  NetworkManager.swift
//  TestNewsApp
//
//  Created by Rafael Nalbandyan on 2/27/21.
//

import Foundation
import Alamofire

protocol NetworkManagerProtocol {
    func get(completionBlock: @escaping (AFDataResponse<Data>) -> Void)
}

final class NetworkManager: NetworkManagerProtocol {
    var session: Session?
    var router: APIRouter?
    
    static var isConnectedToInternet: Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
    
    required init(session: Session, router: APIRouter) {
        self.session = session
        self.router = router
    }
    
    func get(completionBlock: @escaping (AFDataResponse<Data>) -> Void) {
        session?.request(router!).responseData(completionHandler: {data in
            completionBlock(data)
        })
    }
}
