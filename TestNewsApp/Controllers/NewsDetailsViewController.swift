//
//  NewsDetailsViewController.swift
//  TestNewsApp
//
//  Created by Rafael Nalbandyan on 2/27/21.
//

import UIKit
import Alamofire
import RealmSwift

final class NewsDetailsViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var videosCollectionView: UICollectionView!
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var galleryCollectionViewHeightConstaint: NSLayoutConstraint!
    @IBOutlet weak var videosCollectionViewHeightConstaint: NSLayoutConstraint!
    private let imageGalleryXib = UINib(nibName: "ImageGalleryCollectionViewCell", bundle: nil)
    private let videoXib = UINib(nibName: "VideoCollectionViewCell", bundle: nil)
    private var galleryCollectionViewIsHidden: Bool = true
    private var videoCollectionViewIsHidden: Bool = true
    private var imagesGallery: [DataGallery] = []
    private var dataVideos: [DataVideo] = []
    private var galleryHeight: CGFloat = 120
    private var news: DataNews? {
        didSet {
            setUpUI(dataNews: news)
        }
    }
    private var dataStorage: NewsEntity? {
        didSet {
            guard news == nil else { return }
            setUpUI(dataNews: dataStorage)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        toggleCollectionView(collectionViewHeightConstraint: galleryCollectionViewHeightConstaint, hide: true)
        toggleCollectionView(collectionViewHeightConstraint: videosCollectionViewHeightConstaint, hide: true)
        videosCollectionViewHeightConstaint.constant = 0
        galleryCollectionViewHeightConstaint.constant = 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        dataVideos = []
        imagesGallery = []
    }
    
    @IBAction func toggleVideoCollectionView(_ sender: UIButton) {
        videoCollectionViewIsHidden = !videoCollectionViewIsHidden
        toggleCollectionView(collectionViewHeightConstraint: videosCollectionViewHeightConstaint, hide: videoCollectionViewIsHidden)
    }
    
    @IBAction func toggleGalleryCollectionView(_ sender: UIButton) {
        galleryCollectionViewIsHidden = !galleryCollectionViewIsHidden
        toggleCollectionView(collectionViewHeightConstraint: galleryCollectionViewHeightConstaint, hide: galleryCollectionViewIsHidden)
    }
}

// MARK: - Data management functions
extension NewsDetailsViewController {
    // This function should be called when app loaded first time. In this case  we have not any data from local storage
    public func loadData() {
        loadDataFromNetwork()
        if news == nil {
            loadStorageData()
        }
    }
    
    private func loadStorageData() {
        let news = RealmManager().getData()
        self.dataStorage = news.first
    }
    
    private func downloadData(
        _ networkManager: NetworkManagerProtocol,
        completion: @escaping (Result<Data, AFError>) -> Void) {
        networkManager.get(completionBlock: { response in
            completion(response.result)
        })
    }
    
    private func loadDataFromNetwork() {
        let session = Session.default
        let router: APIRouter = JSONPlaceHolderAPIAction.getNews
        downloadData(NetworkManager(session: session, router: router), completion: {response in
            switch response {
            case .success(let data):
                let decoder = JSONDecoder.init()
                let dataNews = try! decoder.decode(NewsResponse.self, from: data)
                guard let news = dataNews.metadata else { return }
                guard let firstNews = news.first else { return }
                self.news = firstNews
            case.failure:
                print ("error")
            }
        })
    }
}

// MARK:- UI setups
extension NewsDetailsViewController {
    private func toggleCollectionView(collectionViewHeightConstraint: NSLayoutConstraint, hide: Bool) {
        UIView.animate(withDuration: 0.2, delay: 0.2, options: UIView.AnimationOptions.curveEaseIn, animations: {
            collectionViewHeightConstraint.constant = hide ? 0 : self.galleryHeight
            self.updateViewConstraints()
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    private func setUpUI(dataNews: DataNews?) {
        guard let news = dataNews else { return }
        setUpNewsCoverImage(coverPhotoUrl: news.coverPhotoUrl)
        self.setUpGalleryCollectionView()
        self.setUpVideosCollectionView()
        let body = news.body ?? "no description"
        self.descriptionLabel.text = body.htmlToString
        self.titleLabel.text = news.title ?? "no title"
        DispatchQueue.main.async {
            self.collectionView.reloadData()
            self.videosCollectionView.reloadData()
        }
    }
    
    private func setUpUI(dataNews: NewsEntity?) {
        guard let news = dataNews else { return }
        setUpNewsCoverImage(coverPhotoUrl: news.coverPhotoUrl)
        self.descriptionLabel.text = news.body.htmlToString
        self.titleLabel.text = news.title
        navigationItem.title = news.category
    }
    
    private func setUpNewsCoverImage(coverPhotoUrl: String?) {
        guard let coverPhotoUrl = coverPhotoUrl else { return }
        self.newsImageView.setImage(fromString: coverPhotoUrl)
    }
    
    private func setUpGalleryCollectionView() {
        guard let dataNews = self.news else { return }
        guard let dataGallery = dataNews.gallery else { return }
        self.imagesGallery = dataGallery
        collectionView.register(imageGalleryXib, forCellWithReuseIdentifier: ImageGalleryCollectionViewCell.id)
    }
    
    private func setUpVideosCollectionView() {
        guard let dataNews = self.news else { return }
        guard let dataVideos = dataNews.video else { return }
        self.dataVideos = dataVideos
        videosCollectionView.register(videoXib, forCellWithReuseIdentifier: VideoCollectionViewCell.id)
    }
    
    // MARK: - collectionview UI setups
    private func setUpGalleryCell(indexPath: IndexPath) -> ImageGalleryCollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageGalleryCollectionViewCell.id, for: indexPath) as! ImageGalleryCollectionViewCell
        let gallery = self.imagesGallery[indexPath.row]
        cell.setUpCell(gallery: gallery)
        return cell
    }
    
    private func setUpVideoCell(indexPath: IndexPath) -> VideoCollectionViewCell {
        let cell = videosCollectionView.dequeueReusableCell(withReuseIdentifier: VideoCollectionViewCell.id, for: indexPath) as! VideoCollectionViewCell
        let video = self.dataVideos[indexPath.row]
            cell.setUpCell(video: video)
        return cell
    }
    
    private func setUpNavigationBar(title: String) {
        navigationController?.navigationItem.title = title
    }
}

// MARK: - UICollectionView DataSource methods
extension NewsDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case self.collectionView:
            return imagesGallery.count
        case self.videosCollectionView:
            return dataVideos.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case self.collectionView:
            return self.setUpGalleryCell(indexPath: indexPath)
        case self.videosCollectionView:
            return setUpVideoCell(indexPath: indexPath)
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case self.collectionView:
            showGallerySlider(selectedIndex: indexPath.row)
        case self.videosCollectionView:
            guard let videoId = dataVideos[indexPath.row].youtubeId else { return }
            self.openVodeo(videoId: videoId)
        default:
            break
        }
    }
}

// MARK: - PopUps setups
extension NewsDetailsViewController: UIPopoverPresentationControllerDelegate {
    private func showGallerySlider(selectedIndex: Int) {
        guard let galleryAlert = galeryModal(dataGallery: imagesGallery, selectedIndex: selectedIndex) else { return }
        DispatchQueue.main.async {
            self.present(galleryAlert, animated: true, completion: nil)
        }
    }
    
    private func galeryModal(dataGallery: [DataGallery], selectedIndex: Int) -> GalleryModalViewController? {
        let galleryModalViewController =
                GalleryModalViewController(nibName: "GalleryModalViewController", bundle: nil)
        galleryModalViewController.dataGallery = dataGallery
        galleryModalViewController.selectedImageIndex = selectedIndex
        return galleryModalViewController
    }
    
    private func openVodeo(videoId: String) {
        DispatchQueue.main.async {
            let newViewController = VideoViewController(nibName: "VideoViewController", bundle: nil)
            newViewController.videoId = videoId
            self.present(newViewController, animated: true, completion: nil)
        }
    }
}

// MARK: - NewsSelectionDelegate methods
extension NewsDetailsViewController: NewsSelectionDelegate {
    func newsSelected(_ dataNews: NewsEntity) {
        self.dataStorage = dataNews
    }
    
    func newsSelected(_ dataNews: DataNews) {
        self.news = nil
        DispatchQueue.main.async {
            self.news = dataNews
        }
    }
}
