//
//  NewsViewController.swift
//  TestNewsApp
//
//  Created by Rafael Nalbandyan on 2/26/21.
//

import UIKit
import Alamofire
import RealmSwift

protocol NewsSelectionDelegate: class {
    func newsSelected(_ dataNews: NewsEntity)
    func newsSelected(_ dataNews: DataNews)
}

final class NewsViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    private var networkDataNews: [DataNews] = []
    private var storageDataNews: [NewsEntity] = []
    private let tableViewCellXib = UINib(nibName: "NewsTableViewCell", bundle: nil)
    private var isConnected: Bool = false
    private var viewedNewsIDs: Set<String> = []
    weak var delegate: NewsSelectionDelegate?
    private let isConnectedToInternet = NetworkManager.isConnectedToInternet
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpTableView()
        checkInternetConnection()
    }
}

extension NewsViewController {
    private func checkInternetConnection() {
        if isConnectedToInternet {
            self.isConnected = true
            self.loadDataFromNetwork()
        } else {
            self.isConnected = false
            DispatchQueue.main.async {
                self.getStorageData()
            }
        }
    }
    
    private func updateDataStorage(dataNews: [DataNews]) {
        deleteFromStorage()
        saveToStorage(dataNews: dataNews)
    }
    
    private func loadDataFromNetwork() {
        let session = Session.default
        let router: APIRouter = JSONPlaceHolderAPIAction.getNews
        
        
        downloadData(NetworkManager(session: session, router: router), completion: {response in
            switch response {
            case .success(let data):
                let decoder = JSONDecoder.init()
                let dataNews = try! decoder.decode(NewsResponse.self, from: data)
                guard let news = dataNews.metadata else { return }
                self.networkDataNews = news
                // We need to update the storage data every time we connect to the internet.
                self.updateDataStorage(dataNews: news)
                self.tableView.reloadData()
            case.failure:
                print ("error")
            }
        })
    }
    
    private func downloadData(
        _ networkManager: NetworkManagerProtocol,
        completion: @escaping (Result<Data, AFError>) -> Void) {
        networkManager.get(completionBlock: { response in
            completion(response.result)
        })
    }
}

extension NewsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard self.isConnected else {
            return storageDataNews.count
        }
        return networkDataNews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NewsTableViewCell.id, for: indexPath) as! NewsTableViewCell
        let dataStorage = self.storageDataNews[indexPath.row]
        cell.seenView.isHidden = true
        let newsId: String = "\(dataStorage._id)"
        
        if isConnected {
            let dataNetwork = self.networkDataNews[indexPath.row]
            cell.setUpCell(dataNetwork: dataNetwork)
        } else {
            // We are trying to use storage data when we have not connection
            if dataStorage.seen {
                self.viewedNewsIDs.insert(newsId)
            }
            cell.setUpCell(dataStorage: dataStorage)
        }
        setUpCellViewState(cell: cell, newsId: newsId)
        return cell
    }
    
    // MARK: - Navigation
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var category: String
        if isConnected {
            let dataNetwork = self.networkDataNews[indexPath.row]
            category = dataNetwork.category ?? ""
            delegate?.newsSelected(dataNetwork)
        }
        let dataStorage = self.storageDataNews[indexPath.row]
        category = dataStorage.category
        delegate?.newsSelected(dataStorage)
        let newsId: ObjectId = dataStorage._id
        RealmManager().updateById(id: newsId, fieldName: "seen", fieldValue: true)
        self.updateSeeNewsStatus(newsId: "\(newsId)")
        if
            let detailViewController = delegate as? NewsDetailsViewController,
            let detailNavigationController = detailViewController.navigationController {
            splitViewController?.showDetailViewController(detailNavigationController, sender: nil)
            detailNavigationController.navigationItem.title = category
        }
    }
}

// MARK:- UI Setups
extension NewsViewController {
    private func setUpTableView() {
        tableView.separatorStyle = .none
        tableView.register(tableViewCellXib, forCellReuseIdentifier: NewsTableViewCell.id)
    }
    
    private func setUpCellViewState(cell: NewsTableViewCell, newsId: String) {
        let isNewsWasViewed: Bool = viewedNewsIDs.contains("\(newsId)")
        cell.seenView.isHidden = !isNewsWasViewed
    }
    
    private func updateSeeNewsStatus(newsId: String) {
        guard !viewedNewsIDs.contains(newsId) else { return }
        self.viewedNewsIDs.insert(newsId)
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}

// MARK:- Storage CRUD
extension NewsViewController {
    private func getStorageData() {
        let news = RealmManager().getData()
        self.storageDataNews = news
        self.tableView.reloadData()
    }
    
    private func saveToStorage(dataNews: [DataNews]) {
        self.storageDataNews = []
        for news in dataNews {
            RealmManager().saveData(news) { (data) in
                self.storageDataNews.append(data)
            }
        }
    }
    
    private func deleteFromStorage() {
        RealmManager().deleteData()
    }
}
