//
//  VideoViewController.swift
//  TestNewsApp
//
//  Created by Rafael Nalbandyan on 2/4/21.
//

import UIKit
import YouTubePlayer

final class VideoViewController: UIViewController {

    @IBOutlet var videoPlayer: YouTubePlayerView!
    public var videoId: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        videoPlayer.delegate = self
        guard let videoID = videoId else { return }
        videoPlayer.loadVideoID(videoID)
    }

    @IBAction func close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension VideoViewController: YouTubePlayerDelegate {
    func playerReady(_ videoPlayer: YouTubePlayerView) {
        videoPlayer.play()
    }
}
