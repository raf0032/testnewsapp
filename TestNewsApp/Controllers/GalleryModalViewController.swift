//
//  GalleryModalViewController.swift
//  TestNewsApp
//
//  Created by Rafael Nalbandyan on 3/5/21.
//

import UIKit

final class GalleryModalViewController: UIViewController {

    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var newsView: UIView!
    
    var dataGallery: [DataGallery] = []
    var selectedImageIndex: Int = 0
    public static let id: String = "GalleryModalViewController"
    private var sliderWasMovedTo: SliderAcrions = .none
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard dataGallery.count > 0 else { return }
        loadImage(index: selectedImageIndex)
    }
    
    @IBAction func moveImages(_ sender: UIButton) {
        guard let title = sender.titleLabel else { return }
        guard let text = title.text else { return }
        switchSlider(title: text)
    }
    
    @IBAction func close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UI setups
    private func switchSlider(title: String) {
        switch title {
        case "<":
            guard selectedImageIndex > 0 else { return }
            sliderWasMovedTo = .left
            selectedImageIndex -= 1
        case ">":
            guard selectedImageIndex < dataGallery.count - 1 else { return }
            sliderWasMovedTo = .right
            selectedImageIndex += 1
        default:
            sliderWasMovedTo = .none
        }
        loadImage(index: selectedImageIndex)
    }
    
    private func loadImage(index: Int) {
        let dataImages = self.dataGallery
        guard let thumbnailUrl = dataImages[index].thumbnailUrl else { return }
        self.newsImageView.setImage(fromString: thumbnailUrl)
    }
}

extension GalleryModalViewController {
    private enum SliderAcrions: String {
        case left
        case right
        case none
    }
}
