//
//  News.swift
//  TestNewsApp
//
//  Created by Rafael Nalbandyan on 2/26/21.
//

import Foundation

struct NewsResponse: Decodable {
    let success: Bool?
//    let errors: []
//    let internal_errors: []
    let metadata: [DataNews]?
}

struct DataNews: Decodable {
    let category: String?
    let title: String?
    let body: String?
    let shareUrl: String?
    let coverPhotoUrl: String?
    let date: Int?
    let gallery: [DataGallery]?
    let video: [DataVideo]?
}

struct DataGallery: Decodable {
    let title: String?
    let thumbnailUrl: String?
    let contentUrl: String?
}

struct DataVideo: Decodable {
    let title: String?
    let thumbnailUrl: String?
    let youtubeId: String?
}
