//
//  Constants.swift
//  TestNewsApp
//
//  Created by Rafael Nalbandyan on 2/26/21.
//

import Foundation
import UIKit

struct Constants {
    
    static let endPoint: String = "https://www.helix.am"
    
    struct Rooting {
        static let getNews: String = "/temp/json.php"
    }
    
    struct CoreData {
        static let GalleriesEntity = "Galleries"
        static let NewsEntity = "News"
    }
}


