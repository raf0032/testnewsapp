//
//  Extension+UIImageView.swift
//  TestNewsApp
//
//  Created by Rafael Nalbandyan on 2/27/21.
//

import Foundation
import UIKit
import Kingfisher

public enum ImageAnimation {
    case flipFromLeft
    case flipFromRight
    case fade
    
    func convert(withDuration duration: TimeInterval) -> ImageTransition {
        switch self {
        case .flipFromLeft:
            return ImageTransition.flipFromLeft(duration)
        case .flipFromRight:
            return ImageTransition.flipFromRight(duration)
        case .fade:
            return ImageTransition.fade(duration)
        }
    }
}

extension UIImageView {
    func setImage(fromURL url: URL) {
        kf.setImage(with: url)
    }
    
    func setImage(fromString string: String?) {
        guard let string = string, let url = URL(string: string) else { return }
        setImage(fromURL: url)
    }
    
    func cancelDownload() {
        kf.cancelDownloadTask()
    }
}
